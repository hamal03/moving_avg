#!/usr/bin/python3

# This script retrieved the daily US price data for Bitcoin from coinmetrics.io
# and builds a timeline graph with the US price as well as a 1, 2 and 4 year
# moving average. A 4-year shifted price graph is also printed to show the yield
# for someone who HODLs their sats for 4 years.
# 
# The graph is intended to orange pill an acquaintance

import sys
import requests
import json
from datetime import datetime

bdata = list()
burl = 'https://community-api.coinmetrics.io/v4/timeseries/asset-metrics'
bapistr = '?assets=btc&frequency=1d&metrics=PriceUSD&start_time='
today = int(int(datetime.now().strftime('%s'))/86400)
loopval = 14247
maxdt = 0
while loopval <= today:
    startdate = datetime.fromtimestamp(loopval*86400).strftime('%F')
    enddate = datetime.fromtimestamp((loopval+100)*86400).strftime('%F')
    newdata = requests.get(burl+bapistr+startdate+'&end_time='+enddate)
    if newdata.status_code != 200:
        print("Getting data from coinmetrics failed")
        sys.exit(1)
    jdata = json.loads(newdata.text)
    for bd in jdata['data']:
        if bd['PriceUSD'] is None: continue
        epdate = int(int(datetime.strptime(bd['time'], '%Y-%m-%dT%H:%M:%S.000000000Z').\
            strftime('%s'))/86400+.5)
        if epdate <= maxdt:
            continue
        else:
            maxdt = epdate
        newentry = (epdate, float(bd['PriceUSD']))
        bdata.append(newentry)
    loopval += 98

csvdata=list()
ma1y = list()
ma2y = list()
ma4y = list()

for row in bdata:
    if row[1] == 0.0: continue
    csvdata.append(list())
    csvdata[-1].append(str(row[0]*86400))
    csvdata[-1].append(str(row[1]))
    ma1y.append(row[1])
    while len(ma1y) > 365:
        ma1y.pop(0)
    if len(ma1y) == 365:
        csvdata[-1].append(str(sum(ma1y)/365))
    else:
        csvdata[-1].append("")
    ma2y.append(row[1])
    while len(ma2y) > 730:
        ma2y.pop(0)
    if len(ma2y) == 730:
        csvdata[-1].append(str(sum(ma2y)/730))
    else:
        csvdata[-1].append("")
    ma4y.append(row[1])
    while len(ma4y) > 1460:
        ma4y.pop(0)
    if len(ma4y) == 1460:
        csvdata[-1].append(str(sum(ma4y)/1460))
        csvdata[-1].append(str(ma4y[0]))
    else:
        csvdata[-1] += ["",""]

mvdata = open("moving.csv", "w")
for line in (range(len(csvdata))):
    mvdata.write(",".join(csvdata[line])+"\n")
mvdata.close()
